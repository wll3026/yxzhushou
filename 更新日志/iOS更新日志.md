# iOS App更新日志
<!-- ## v1.0.0(*) 正在修复
日期：2021/

新增：
* 修复开发模式多手机同时开发导致的bug
 -->

## v1.0.7
日期：2022/5/22
* 新增 
	* [showUI](#showui) 实时获取AppUI编辑框返回参数
	* [WriteClipboard](#writeclipboard) 文本写入剪切板 `仅支持部分系统` 
* 新增 开发模式会自动清空其他项目本地文件
* 优化 LuaPanda.lua 关闭attach模式，开发更稳定
* 修复 lua_exit() 函数 开发模式导致vscode连接异常的bug

## v1.0.6
日期：2022/4/29
* 新增 
	* [getColor](#getcolor) 获取屏幕某点颜色值
	* [getUUID](#getuuid) 设备唯一标识符(卸载重置，非UDID)
	* [base64encode](#base64encode) Base64编码
	* [base64decode](#base64decode) Base64解码
	* [aesEncrypt](#aesencrypt) AES加密
	* [aesDecrypt](#aesdecrypt) AES解密
	* [getName](#getname) 用户定义的手机名称（别名）
	* [getSystemName](#getsystemname) 手机设备名称
	* [getSystemVersion](#getsystemversion) 手机系统版本号
	* [getScreenScale](#getscreenscale) 获取手机实际分辨率（竖屏）
	* [getScreenBounds](#getscreenbounds) 获取手机物理尺寸（竖屏）
* 修复 lua_exit() 函数 开发模式下的bug
* 修复代码格式错误时连接VScode异常的问题
* 修复 zip 项目链接下载问题
* 新增编辑框输入法显示（防遮挡）
* 新增物联网代码托管(免费)：云更新文件重命名标准：必须有后缀名、不允许中文命名
* 新增 lua 文件云加密算法
* 新增 Log 错误日志记录
* 优化 IPA 定时器
* 新增 UI 可视化，兼容叉叉助手 UI.json 格式

## v1.0.5
日期：2022/4/16
* 修复网络错误导致激活验证失败

## v1.0.4
日期：2022/3/25
* 新增 App激活有效期

## v1.0.2
日期：2022/3/19
* 修复 非开发模式下无法运行程序问题
* 每次打开项目，会自动检测是否已经允许联网
* 删除UI界面的下载按钮（下载的项目无法被录屏识别到路径，改为项目链接按钮）
* 修复 链接不安全情况下无法下载的问题（已开放权限）[iOS HTTP安全权限设置](https://blog.csdn.net/panjican/article/details/51314307)
```
下载时，项目链接不安全，提示该信息。
报错信息：
# App Transport Security has blocked a cleartext HTTP (http://) resource load since it is insecure
```

## v1.0.1
* 修复 string.format("%x", -1000) 无法识别负数问题(临时解决方案)
* 待修复bug string.format("%04x", -1000) 无法截取后4位
```c
//lstrlib.c 785行
case 'o':  case 'u':  case 'x':  case 'X': {
	addintlen(form);
	sprintf(buff, form, (LUA_INTFRM_T)luaL_checknumber(L, arg)); //临时解决方案
	 //sprintf(buff, form, (unsigned LUA_INTFRM_T)luaL_checknumber(L, arg));
```
* 新增 函数 [WriteClipboard]() 文本写入剪切板
* 新增 全局变量 YXZHUSHOU_UI_TEXT
* 删除触摸函数，改为物联网鼠标

## v1.0.0(50)
日期：2021/12/12

新增：
+ 函数 [binarizeImage]() 二值化转table

修复：
+ 解决zip包解压中文乱码问题

## v1.0.0(36)
日期：2021/08/16

新增：
+ 自动调用LuaPanda.lua，无需手动输入
+ 图标更改为 鱼叉助手logo
+ app名称更改为 鱼叉助手

## v1.0.0(35) 重要修改
日期：2021/08/4

新增：
+ 与vscode udp连接
+ vscode 调试
+ lua socket C源码库
+ lua5.3 改为 lua5.1
+ 优化 udp 连接效率

bug：
+ 修复 lua 5.1 源码 string.format("%04x", -3000) 负数无法计算问题
+ 修复 [getScreenSize](../鱼叉助手API文档.md/#getScreenSize) 获取分辨率失败时参数为0

## v1.0.0(28)
日期：2021/04/18

新增：（需求作者：79324158）
+ 函数 [http](../鱼叉助手API文档.md/#http) HTTPget/HTTPpost 网络请求
+ 函数 [base64encode](../鱼叉助手API文档.md/#base64encode) base64编码
+ 函数 [base64decode](../鱼叉助手API文档.md/#base64decode) base64解码

## v1.0.0(27)
日期：2021/04/18

核心优化：
+ 函数 [findColor](../鱼叉助手API文档.md/#findColor)、[findColors](../鱼叉助手API文档.md/#findColors) 新增模糊找色算法，提高16倍以上识别速度。

新增：
+ 函数 [mTime](../鱼叉助手API文档.md/#mTime) 毫秒级时间戳

bug：
+ 修复 一键运行 自动下载失败的bug（对应 智图学院PC端（1.0.5版本）开发模式）

## v1.0.0(26) 重要更新
日期：2021/04/16

新增：
+ 找色算法深度优化，并增加更多丰富功能
+ 升级函数 [findColor](../鱼叉助手API文档.md/#findColor) 区域多点找色（支持偏色、精度值、找色方向设置）
+ 函数 [keepScreen](../鱼叉助手API文档.md/#keepScreen) 保持屏幕
+ 函数 [getColorRGB](../鱼叉助手API文档.md/#getColorRGB) 获取颜色RGB值
+ 升级函数 [findColors](../鱼叉助手API文档.md/#findColors) 高级区域多点找色（支持偏色、精度值、找色方向设置）
+ 对应 智图学院PC端（1.0.5版本）

删除：
+ 函数 ~~[findColor_vs](../鱼叉助手API文档.md/#findColor_vs) 固定位置找色~~（ 推荐 [getColorRGB](../鱼叉助手API文档.md/#getColorRGB) ）

## v1.0.0(25)
日期：2021/04/15

bug：
+ 修复 App UI 项目列表 添加 删除 序号错乱问题
+ 增加 App UI 项目列表 删除功能
+ 对应 智图学院PC端（1.0.5版本）

## v1.0.0(24)
日期：2021/04/13

新增：（需求作者：79324158）
+ 函数 [lua_exit](../鱼叉助手API文档.md/#lua_exit) 退出脚本程序

bug：
+ 修复函数 [touchDown](../鱼叉助手API文档.md/#touchDown) 自动连接设备失败 死循环导致开发模式无法停止运行的问题
+ 修复函数 [mSleep](../鱼叉助手API文档.md/#mSleep) 延时 开发模式无法停止运行的问题
+ UI刘海自动适配代码重写
+ 对应 智图学院PC端（1.0.5版本）

## v1.0.0(23)
日期：2021/04/12

新增：(需求作者：357339600)
+ 开发模式支持一键运行、一键下载、一键截图
+ 开发模式自动解压缩
+ 对应 智图学院PC端（1.0.5版本）

## v1.0.0(15)
日期：2021/03/16

bug：
+ 修复下载地址无法保存的bug.(反馈作者：357339600)

## v1.0.0(10) 重要更新
日期：2021/01/31

bug：
+ 修复运行时长2小时就iOS后台杀掉的bug，新版测试稳定超过24小时。

## v1.0.0(9)
日期：2021/01/29

新增：
+ 主页UI增加 测试鼠标 功能，测试设备是否连接正常。
+ [showUI](../鱼叉助手API文档.md/#showUI) 页面优化，新增单选框，支持背景图片，支持读取叉叉json文件

bug：
1. Lua API [UIshow](../鱼叉助手API文档.md/#UIshow) 更名为 [showUI](./鱼叉助手API文档.md/#showUI) ,兼容叉叉
2. [findColor_vs](../鱼叉助手API文档.md/#findColor_vs) 比色函数逻辑增强

## v1.0.0(8)
日期：2021/01/22

新增：
+ 自动下载功能会提前删除旧版本目录，保证程序运行最新程序，解决Lua代码没有更新的bug。

bug：
+ 程序下载成功，但Lua代码没有更新的bug。
+ 修复鼠标初始化，只有移动函数才会激活鼠标初始化，不使用移动不会进行初始化。

## v1.0.0(6)
日期：2021/01/20

新增：
+ ~~[findColor_vs](../鱼叉助手API文档.md/#findColor_vs) 多点比色函数~~
+ [init](../鱼叉助手API文档.md/#init) 初始化方向函数，允许横屏取色,横屏点击
+ App UI 增加开发者发送图片可选项

## v1.0.0(4)
日期：2021/01/20

新增：
+ [教程](https://www.bilibili.com/video/BV1qt4y1z7Rw)
+ 启动直播录屏会先调用touchUp函数进行物联设备连接一次
+ 使用了touchMove,touchup内部会调用touchinit鼠标位置初始化(左下角)