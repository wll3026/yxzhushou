# 鱼叉助手 App API

![输入图片说明](mdimg/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8B%E5%8E%9F%E7%90%86%E5%9B%BE.png)

### `鱼叉助手 版本号：v1.0.7`

##### 鱼叉助手 App 简介：
 * 鱼叉助手 App 脚本使用 Lua 语言进行编写，支持 Lua5.1 的所有语法与基本函数，配合脚本特有的函数命令，实现找图、找色等高级功能。
* 鱼叉助手 App 使用系统权限运行 Lua 脚本，录屏启动后 App 关闭也不影响程序运行，可保证24小时以上稳定运行，支持热更新，远程控制。

##### VScode项目目录结构：
```
├─dome1		(以main.lua为主的项目名称,暂不支持中文命名)
│  ├─main.lua	(【必选】主程序,当前位置的目录名称 dome1 为项目名称)
│  ├─abc		(【可选】自定义目录 abc)
│  │  ├─a.txt
│  │  ├─b.lua         (lua文件调用方法：require("abc.b"))
│  │  └─等等
│  ├─abcd.lua   (【可选】main.lua同级自定义文件lua文件调用方法：require("abcd"))
│  ├─socket		(【必选】网络通讯 Luasocket 库,文档内下载)
│  │  ├─ftp.lua       (lua文件调用方法：require("socket.ftp"))
│  │  ├─headers.lua   (lua文件调用方法：require("socket.headers"))
│  │  ├─http.lua      (lua文件调用方法：require("socket.http"))
│  │  ├─ltn12.lua     (lua文件调用方法：require("socket.ltn12"))
│  │  ├─mbox.lua      (lua文件调用方法：require("socket.mbox"))
│  │  ├─mime.lua      (lua文件调用方法：require("socket.mime"))
│  │  ├─smtp.lua      (lua文件调用方法：require("socket.smtp"))
│  │  ├─socket.lua    (lua文件调用方法：require("socket.socket"))
│  │  ├─tp.lua        (lua文件调用方法：require("socket.tp"))
│  │  └─url.lua       (lua文件调用方法：require("socket.url"))
│  ├─UI.json	(【可选】可视化UI参数文件,兼容叉叉助手格式)
│  ├─Mouse.lua	(【必选】物联网鼠标 Lua 库,文档内下载)
│  └─JSON.lua	(【必选】 JSON Lua库,文档内下载)
│ 
├─dome2 (其他项目示例)
│  └─main.lua
├─dome3 (其他项目示例)
│  └─main.lua
├─dome4 (其他项目示例)	
│  └─main.lua

```

##### 新手指导：

[【入门课】iOS免越狱 Lua VScode环境配置教学](https://www.bilibili.com/video/BV1DU4y177JN?spm_id_from=333.999.0.0)

* 关于app出现闪退问题（2022/6/21）：重新扫码安装APP

* 关于 无法验证应用 需要互联网连接以验证是否信任开发者 问题：
	* 1、还原手机网络设置：前往设置-通用-还原-还原网络设置。  
	* 2、苹果id的问题，换个苹果id登录尝试。  
	* 3、断网后打开设置清除浏览器缓存再打开app，然后打开网络尝试。  
	* 4、卸载重装该APP尝试。
	* 原因：如果某个应用无法启动且弹出“无法验证应用”提示，这种情况就是俗称的“掉证书”。除了 App Store 以外，在 iOS 设备上安装应用还有三种方式：个人开发者签名安装、企业开发者签名安装、个人普通 Apple ID 真机调试安装，依靠这三种方法安装的应用都会有使用时间限制，“掉证书”指的就是超过使用时限或者签名被苹果提前取消。
 ![输入图片说明](mdimg/ios/1563760966574013629.jpg)

* 关于app第一次安装使用问题：
第一次安装，一定要等录屏倒计时开始，弹出网络授权窗口，允许网络服务，才可以关闭录屏界面。否则iOS就直接默认脚本是不允许网络。如果没有网络授权弹窗，必须手动去手机卸载app，清空所有数据，不能用爱思助手去卸载。重装以后，再重复前面的操作，第一次一定要有弹窗，才说明脚本可以启动网络服务。（这是苹果的坑，第一次使用不要点太快。有的操作太快，录屏还没倒计时就关闭了录屏返回了到App，导致网络权限弹窗无法正常显示，就以为App有问题了）
* VScode开发问题：
	* 关于 VScode 开发时无法搜索到 App 解决方案1：
		* 网络授权：iOS 设置 -> 鱼叉助手 -> 无线数据 -> 启动WIFI和蜂窝数据
		* 时间问题：手机时间是否与电脑时间一致
		* 网关问题：https://www.xitongbuluo.com/jiaocheng/1649.html
	* 关于 VScode 开发时无法运行脚本：查看代码问题诊断信息，代码有红色标记错误则无法正常调试
	* vscode多次运行测试，会导致App截图卡顿，运行卡顿，重启录屏就可以解决。
	* 关于 app 无法安装：app需要签名才可以安装
	* 关于 录屏 运行脚本闪退问题：检查代码是否内存溢出，代码质量差会导致程序异常
	* 关于 主页界面 不是纯白问题：关闭手机深夜模式。
	
		![输入图片说明](mdimg/ios/QQ%E5%9B%BE%E7%89%8720220310161209.png)
	 * 关于 VScode 取色器不显示生成格式问题：
		 * lua.exe缺少dll文件，自行百度下载该文件即可解决。
		![输入图片说明](mdimg/ios/QQ%E5%9B%BE%E7%89%8720220427150602.png)
		![输入图片说明](mdimg/ios/QQ%E5%9B%BE%E7%89%8720220427150549.png)
* 如何使用鱼叉助手的项目链接功能？
	* 1、打开vscode 鱼叉集成环境会自动设置电脑为局域网服务器（如果自己有服务器则不需要使用vscode）
	* 2、测试本地服务器是否在线，在浏览器输入电脑的本地ip，会显示vscode本地所在目录则说明服务器启动成功。
	* 3、如果关闭vscode，服务器功能则会自动关闭。
	* 4、将项目整个文件夹打包成zip格式（vscode文件列表右上角也有一个发布功能，自动生成当前项目为zip，并保存到服务器目录内）
	* 5、将zip包名改为与项目不同名（因为点击运行代码，会自动删除同名zip），改名后才可以避免被删除。
	* 6、项目链接格式：http:// 电脑ip(或服务器ip) + 文件名.zip
	* 7、如何测试项目链接可正常运行，把项目链接输入到电脑浏览器（或者手机浏览器），能直接下载到服务器上面的zip文件则说明成功。
	* 8、鱼叉助手APP必须关闭开发模式按钮。
	* 9、鱼叉助手APP打开项目链接按钮则会每次启动录屏都会优先删除原有的代码，再去自动下载zip，注意：如果服务器不在线，下载失败则不会运行代码，并且原来已经下载的代码被删除。
	* 10、鱼叉助手APP关闭项目链接按钮则会直接运行已经下载好的zip。

	* 物联网代码托管

```json
例子：
{"ID":"12345678","APIkey":"xzzcxk5ldsGsX00LzdELVEPWo="}
```

* 开发物联网鼠标技巧：
	* 鼠标点击后显示如果遮挡找色功能，可以使用Mouse:MouseMove移动到其他位置，只要不影响找色即可，也可以移动回左上角(x = -3000，y = -3000)
* 关于录屏截图问题：
	* 对于录屏截图时忽大忽小问题是由于手机接入了鼠标，导致部分手机功能异常，需要鼠标保持持续移动（可使用电脑鼠标进行测试，使用 getScreenSize() 函数来判断），即可恢复正常录屏截图。  


##### 技术支持：
* 反馈问题使用 [码云issues](https://gitee.com/lua_development/yxzhushou/issues) 留言，我们会关注和回复。
* 鱼叉助手 iOS App [更新日志](https://gitee.com/lua_development/yxzhushou/blob/master/%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97/iOS%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97.md)
* 人鱼情未了 QQ：2787283623
* 物联网交流 QQ群：715467974
* 鱼叉助手 App扫码下载：

![输入图片说明](mdimg/app%E4%B8%8B%E8%BD%BD.png)

#### 全局变量

* [YXZHUSHOU_UI_TEXT](#yxzhushou_ui_text) 获取App UI 编辑框启动录屏前的文本内容（推荐使用 [showUI](#showui) 实时获取 UI 编辑框文本内容）
* [YXZHUSHOU_MAIN_PATH](#yxzhushou_ui_text) 项目 main.lua 文件路径

#### 触摸函数

* [物联网鼠标函数](#物联网鼠标函数) 物联网鼠标
* (已弃用)~~[touchDown](#touchDown) 触摸按下~~
* (已弃用)~~[touchMove](#touchMove) 移动~~
* (已弃用)~~[touchUp](#touchUp) 触摸抬起~~

#### 图像函数
* [init](#init) 初始化屏幕找色方向
* [findColor](#findcolor) 区域多点找色<font color=#ffff00>（推荐使用）</font>
* [findColors](#findcolors) 高级区域多点找色<font color=#ffff00>（推荐使用）</font>
* [getColor](#getcolor) 获取屏幕某点颜色值
* [getColorRGB](#getcolorrgb) 获取屏幕某点颜色R,G,B值 [视频教程](https://www.bilibili.com/video/BV1SW411C7u3?p=3)
* [keepScreen](#keepscreen) 保持屏幕
* [binarizeImage](#binarizeImage) 二值化图片转换为table [视频教程](https://www.bilibili.com/video/BV1jK4y1T7Rh?p=8)

#### 其他函数

* [mSleep](#msleep) 延时
* [sysLog](#syslog) 系统日志
* [lua_exit](#lua_exit) 退出脚本执行
* [base64encode](#base64encode) Base64编码
* [base64decode](#base64decode) Base64解码
* [aesEncrypt](#aesencrypt) AES加密
* [aesDecrypt](#aesdecrypt) AES解密
* [showUI](#showui) 实时获取App UI 编辑框文本内容

#### 设备函数

* [getScreenSize](#getscreensize) 获取实时录屏截图分辨率（竖屏）
* [getMobilephoneType](#getmobilephonetype) 获取当前手机型号
* [WriteClipboard](#writeclipboard) 文本写入剪切板 `仅支持部分系统` 
* [mTime](#mtime) 获取手机本地时间戳（毫秒）
* [getUUID](#getuuid) 设备唯一标识符(卸载重置，非UDID)
* [getName](#getname) 用户定义的手机名称（别名）
* [getSystemName](#getsystemname) 手机设备名称
* [getSystemVersion](#getsystemversion) 手机系统版本号
* [getScreenScale](#getscreenscale) 获取手机实际分辨率（竖屏）
* [getScreenBounds](#getscreenbounds) 获取手机物理尺寸（竖屏）

#### 网络函数

socket.lua [扩展库下载 ](https://gitee.com/lua_development/yxzhushou/tree/master/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8BLua%E7%AC%AC%E4%B8%89%E6%96%B9%E5%BA%93)

* [socket.tcp](#socket) LuaSocket TCP协议
* [socket.udp](#socket) LuaSocket UDP协议
* [socket.http](#socket.http) LuaSocket HTTP协议 [视频教程](https://www.bilibili.com/video/BV1bi4y127p6?spm_id_from=333.999.0.0)

#### 物联网鼠标函数

Mouse.lua [扩展库下载 ](https://gitee.com/lua_development/yxzhushou/tree/master/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8BLua%E7%AC%AC%E4%B8%89%E6%96%B9%E5%BA%93)  [物联网鼠标API文档](https://gitee.com/lua_development/yxzhushou/blob/master/%E7%89%A9%E8%81%94%E7%BD%91%E9%BC%A0%E6%A0%87API%E6%96%87%E6%A1%A3.md)

* [Mouse:new](#mousenew) 初始化设备配置 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:MouseDown](#mousemousedown) 鼠标按下 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:MouseMove](#mousemousemove) 鼠标相对移动 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:MouseUp](#mousemouseup) 鼠标抬起 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:MouseClick](#mousemouseclick) 鼠标单击/长按 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:MouseSlide](#mousemouseslide) 鼠标滑动 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:KeyboardClick](#mousekeyboardclick) 键盘输入 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:touchMove](#mousetouchmove) 鼠标绝对移动 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)
* [Mouse:touchSlide](#mousetouchslide) 鼠标滑动（自定义示例，局域网推荐使用）[视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

#### UI 可视化

UI.json [UI示例 ]()
* [Mouse:new](#mousenew) 初始化设备配置 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

# init

### **初始化屏幕找色方向**

参数|类型|说明
-|-|-
appid|<font color=#FF8C00>string</font>|兼容叉叉，暂时无用
rotate|<font color=#00FFFF>number</font>|0 - 竖屏，1 - Home在右，~~2 - Home在左~~

**参数说明：**

+ 假如使用竖屏对截图进行取色，需要告诉脚本 init("0",0) 开发取色方向为竖屏，如果实际运行时，当前为横屏与开发不一致，系统会对每个受影响函数的坐标参数转换为横屏坐标点，当前屏幕方向与开发方向一致则不作修改。

返回值|类型|说明
-|-|-
nil|nil|nil

**语法：**

```lua
init(appid, rotate)
```

**脚本示例：**

```lua
--开发方向为竖屏
init("0", 0);

--开发方向为横屏 home 在右
init("0", 1);
```

受 [init](#init) 函数影响的函数：
+ [findColor](#findColor) 区域多点找色
+ [findColors](#findColors) 区域多点找色
+ [getColor](#getColor) 获取屏幕某点颜色值
+ [getColorRGB](#getColorRGB) 获取屏幕某点颜色R,G,B值

# findColor

### **区域多点找色<font color=#ffff00>（推荐使用）</font>**

参数|类型|说明
-|-|-
left, top|<font color=#00FFFF>number</font>|`[必填]` 寻找区域左上角顶点屏幕坐标
right, bottom|<font color=#00FFFF>number</font>|`[必填]` 寻找区域右下角顶点屏幕坐标
x0,y0|<font color=#00FFFF>number</font>|`[必填]` 起始点坐标值，填写0,0时使用相对坐标体系，填非0,0的绝对坐标自动换算为相对坐标
color|<font color=#00FFFF>number</font>|`[必填]` 起始点颜色的十六进制颜色值
x1,y1|<font color=#00FFFF>number</font>|`[必填]` 偏移位置的坐标值
color1|<font color=#00FFFF>number</font>|`[必填]` 偏移位置需要匹配颜色的十六进制颜色值
degree1|<font color=#00FFFF>number</font>|\[选填\] 偏移位置找色相似度，范围：1 ~ 100，当是100时为完全匹配
offset1|<font color=#00FFFF>number</font>|\[选填\] 偏移位置找色偏色值，十六进制颜色值，当是0x000000时为完全匹配，当大于0时degree1自动设置为100
degree|<font color=#00FFFF>number</font>|\[选填\] 全局找色相似度，范围：1 ~ 100，当是100时为完全匹配,默认为95
hdir|<font color=#00FFFF>number</font>|\[选填\] 水平搜索方向，0表示从左到右，1表示从右到左，默认为0
vdir|<font color=#00FFFF>number</font>|\[选填\] 垂直搜索方向，0表示从上到下，1表示从下到上，默认为0
priority|<font color=#00FFFF>number</font>|\[选填\] 搜索优先级，0表示水平优先，1表示垂直优先，默认为0

**参数说明：**

* 起始点坐标值填写0,0时，偏移位置坐标值使用相对坐标；填写为非0,0的坐标时，则认为偏移位置坐标为绝对坐标，找色时，将根据填写的绝对坐标换算出的相对坐标进行寻找。
* 偏移位置颜色的偏色值或相似度可任意选用，同时填写了偏色值和相似度时，将以偏色为准，忽略相似度值。
* 个别偏移位置颜色偏色值或相似度优先于全局找色相似度，全局找色相似度对未指定偏色或相似度的颜色有效。

返回值|类型|说明
-|-|-
x, y|<font color=#00FFFF>number</font>|`[成功]` 返回颜色第一个坐标，`[失败]` 返回 -1，-1

**关于搜索方向：**

hdir|vdir|priority|区域搜索路径
-|-|-|-
0|0|0|左上角 → 右上角 → 左下角 → 右下角
0|0|1|左上角 → 左下角 → 右上角 → 右下角
0|1|0|左下角 → 右下角 → 左上角 → 右上角
0|1|1|左下角 → 左上角 → 右下角 → 右上角
1|0|0|右上角 → 左上角 → 右下角 → 左下角
1|0|1|右上角 → 右下角 → 左上角 → 左下角
1|1|0|右下角 → 左下角 → 右上角 → 左上角
1|1|1|右下角 → 右上角 → 左下角 → 左上角

**语法：**

+ 第一种语法:

```lua
local x, y = findColor(
    {left, top, right, bottom},
    "x0|y0|color0,x1|y1|color1,x2|y2|color2,...",
    degree,
    hdir,
    vdir,
    priority
    )
```

+ 第二种语法:

```lua
local x, y = findColor(
    {left, top, right, bottom},
    "x0|y0|color0,x1|y1|color1(|degree1),x2|y2|color2(-offset2),...",
    degree,
    hdir,
    vdir,
    priority
    )
```

+ 第三种语法:

```lua
local x, y = findColor(
    {left, top, right, bottom},
    {
        {x = x0,y = y0,color = color0},
        {x = x1,y = y1,color = color1,(degree = degree1)},
        {x = x2,y = y2,color = color2,(offset = offset2)},
        ...
    },
    degree,
    hdir,
    vdir,
    priority
    )
```
**语法说明：**

+ 第一种语法和第二种语法最终都会自动转换成第三种语法

**脚本示例：**

+ 相对坐标写法：

```lua
--第一个坐标(0,0)，系统则判断为相对坐标
local x, y = findColor(
    {0, 0, 639, 959},
    "0|0|0x181F85,29|1|0x00BBFE|90,103|-4|0x0B6BBE-0x050505,65|9|0x150972"
    )
local x, y = findColor(
    {0, 0, 639, 959},
    {
        {x = 0, y = 0, color = 0x181F85},
        {x = 29, y = 1, color = 0x00BBFE, degree = 90},
        {x = 103, y = -4, color = 0x0B6BBE, offset = 0x050505},
        {x = 65, y = 9, color = 0x150972}
    })
```

+ 绝对坐标写法：

```lua
--第一个坐标(268,802)非(0,0)则系统判断为绝对坐标，会自动转换成(0,0)，再根据第一个坐标对其他位置进行相对坐标转换
local x, y = findColor(
    {0, 0, 639, 959},
    "268|802|0x181F85,297|803|0x00BBFE|90,371|798|0x0B6BBE-050505,333|811|0x150972"
)
local x, y = findColor(
    {0, 0, 639, 959},
    {
        {x = 268, y = 802, color = 0x181F85},
        {x = 297, y = 803 color = 0x00BBFE, degree = 90},
        {x = 371, y = 798 color = 0x0B6BBE, offset = 0x050505},
        {x = 333, y = 811 color = 0x150972}
    })
```

+ 单点偏色的写法：

```lua
--如果设置偏色，会优先使用偏色值作为相似度
local x, y = findColor(
    {0, 0, 749, 1333}, 
    "0|0|0xf12735-0x202002,387|-553|0x3eeb5d-0x555555,268|-148|0x2178fa",
    95, 0, 0, 0)
```

+ 单点相似度的写法：

```lua
--如果设置单点相似度80，会优先使用80
local x, y = findColor(
    {0, 0, 749, 1333}, 
    "0|0|0xf12735|80,387|-553|0x3eeb5d|75,268|-148|0x2178fa",
    95, 0, 0, 0)
```
提高 [findColor](#findColor) 函数找色效率：
+ [keepScreen]() 保持屏幕

# findColors

### **高级区域多点找色<font color=#ffff00>（推荐使用）</font>**

参数|类型|说明
-|-|-
参考 [findColor](#findColor)|参考 [findColor](#findColor)|参考 [findColor](#findColor)

返回值|类型|说明
-|-|-
point|<font color=#FFFF00>table</font>|`[成功]` 返回所有符合条件的参照点的坐标，最多99个，`[失败]` 返回空表 { }

**脚本示例：**

```lua
local point = findColors(
    {0, 0, 639, 959},
    "268|802|0x181F85,297|803|0x00BBFE|90,371|798|0x0B6BBE-050505,333|811|0x150972"
)

--返回的 table 为key-value的形式，如下：
--成功
point = {
 {x = 100,y = 110},
 {x = 200,y = 210},
 {x = 300,y = 310},
 ...
}
--失败
point = {}
```

提高 [findColors](#findColors) 函数找色效率：
+ [keepScreen]() 保持屏幕

# getColor

### **获取屏幕某点颜色值**

参数|类型|说明
-|-|-
x，y|<font color=#00FFFF>number</font>|获取颜色值的屏幕坐标

返回值|类型|说明
-|-|-
color|<font color=#00FFFF>number</font>|该点的十进制颜色值RGB

**语法：**

```lua
local color = getColor(x, y)
```

**脚本示例：**

+ 如果某点符合某颜色则点击

```lua
if getColor(100, 100) == 0xffffff then 
   touchDown(1, 100, 100)
   mSleep(50)
   touchUp(1, 100, 100)
end
```
**注意事项：**

* [getColor](#getColor) 函数获得的颜色值十六进制文本中，实际顺序为RGB

# getColorRGB

### **获取屏幕某点颜色R,G,B值**

参数|类型|说明
-|-|-
x，y|<font color=#00FFFF>number</font>|获取颜色值的屏幕坐标

返回值|类型|说明
-|-|-
r，g，b |<font color=#00FFFF>number</font>|该点的十进制颜色值RGB

**语法：**

```lua
local r, g, b = getColorRGB(x, y)
```

**脚本示例：**

+ 判断某点的颜色与某颜色相似：

```lua
local r,g,b = getColorRGB(100,100); --获取该点的R,G,B值
if r > 200 and b < 150 then   --判断颜色强度
    touchDown(1,100,100)
    mSleep(50)
    touchUp(1,100,100)
end
```

+ 封装一个单点模糊比色函数：

```lua
function isColor(x,y,c,s)   --x,y为坐标值，c为颜色值，s为相似度，范围0~100。
    local fl,abs = math.floor,math.abs
    s = fl(0xff*(100-s)*0.01)
    local r,g,b = fl(c/0x10000),fl(c%0x10000/0x100),fl(c%0x100)
    local rr,gg,bb = getColorRGB(x,y)
    if abs(r-rr)<s and abs(g-gg)<s and abs(b-bb)<s then
        return true
    end
    return false
end
if isColor(963,  961, 0x7b593f,90) then     
    touchDown(963, 961)
    mSleep(50)
    touchUp(963, 961)
end
```

+ 封装一个多点比色函数：

```lua
function isColors(color,s) --固定坐标 多点比色
	s = s or 95  --相似度
	s = math.floor(0xff*(100-s)*0.01)    --浮点数
	for var = 1, #color do
		local lr,lg,lb = getColorRGB(color[var][1],color[var][2])   --游戏颜色
		local rgb = color[var][3]   --脚本颜色
		local r = math.floor(rgb/0x10000)    --脚本颜色RGB
		local g = math.floor(rgb%0x10000/0x100)
		local b = math.floor(rgb%0x100)
		if math.abs(lr-r) > s or math.abs(lg-g) > s or math.abs(lb-b) > s then  --绝对值
			return false
		end
	end
	return true
end

--使用
local color = {
  {540,498,0xffffff},
  {519,235,0x0a67c9},
  {607,238,0x0d6fd2}
}
keepScreen(true);--开  搭配保持屏幕，可提高效率
if isColors(color,95) then
  print("找到")
end
keepScreen(false);--关
```

**注意事项：**

* [getColorRGB](#getColorRGB) 函数获得的颜色值十六进制文本中，实际顺序为RGB

# keepScreen

### **保持屏幕**

参数|类型|说明
-|-|-
bool|<font color=#00FF00>boolean</font>| 启动 = true ，关闭 = false

返回值|类型|说明
-|-|-
nil|nil|nil

**语法：**

```lua
keepScreen(bool)
```

**脚本示例：**

```lua
keepScreen(true); --停止截图，保留第一次截图
for y = 1, 640, 10 do
    for x = 1, 960, 10 do
        --格式化颜色为十六进制文本
        local color = string.format("%X", getColor(x, y));
        --输出
        print("("..x..", "..y..") Color: "..color..".");
    end
end
keepScreen(false); --恢复截图
```

受 [keepScreen](#keepScreen) 影响函数：
+ [findColor](#findColor) 区域多点找色
+ [findColors](#findColors) 高级区域多点找色
+ [getColor](#getColor) 获取屏幕某点颜色值
+ [getColorRGB](#getColorRGB) 获取屏幕某点颜色R,G,B值

# binarizeImage

### **二值化图片转换为table**

参数|类型|说明
-|-|-
rect|<font color=#FFFF00>table</font>|`[必填]` {x1, y1, x2, y2} 屏幕二值化的识别范围，越准确越好
diff|<font color=#FFFF00>table</font>|`[必填]` {"颜色1-偏色1", "颜色-偏色2", ...} 色值范围，可以提供多个，供二值化使用

返回值|类型|说明
-|-|-
colorTbl|<font color=#FFFF00>table</font>|图像二值化后的table，0 代表黑色，1 代表白色

**语法：**

```lua
 local colorTbl = binarizeImage({
   rect = {x1, y1, x2, y2},
   diff = {diff}
  })
```

**脚本示例：**

```lua
local colorTbl = binarizeImage({
  rect = {30, 80, 53, 101},
  diff = {"0xf7d363-0x1f1f1f", "0xefaa29-0x1f1f1f"}
})
-- 使用打印表函数输出colorTbl结果
ptable(colorTbl)
--[[
colorTbl格式类似这样：
{
    {0,0,0,0,1,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,0,1,0,0,0,0,0,0,0,0},
    {0,0,0,0,1,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,0,0,0,0,0,0,0,0,0,0},
    {0,0,1,1,0,0,0,0,0,0,0,0,0,0},
    {0,1,0,1,0,0,0,0,0,0,0,0,0,0},
    {0,1,1,1,1,1,0,1,1,0,0,0,0,0},
    {1,0,1,1,0,0,0,0,1,1,1,0,0,0},
    {0,1,0,1,0,0,0,0,0,0,1,1,0,0},
    {1,1,1,0,0,0,0,0,0,0,1,1,0,0},
    {1,0,0,0,0,0,0,0,0,0,1,0,1,0},
    {1,0,0,0,0,0,0,0,0,0,0,1,1,0},
    {1,0,0,0,0,0,0,0,0,0,0,1,0,1},
    {1,0,0,0,0,0,0,0,0,0,0,0,1,0},
    {1,0,0,0,0,0,0,0,0,0,0,1,1,1},
    {1,0,0,0,0,0,0,0,0,0,0,1,0,1},
    {1,1,0,0,0,0,0,0,0,0,0,1,1,0},
    {1,0,1,0,0,0,0,0,0,0,1,0,0,0},
    {0,1,1,1,0,1,0,0,1,1,1,0,0,0},
    {0,0,0,1,1,1,0,1,1,0,0,0,0,0},
    {0,0,0,0,0,0,1,0,1,0,0,0,0,0}
}
]]
```

# mSleep

### **延时**

参数|类型|说明
-|-|-
interval|<font color=#00FFFF>number</font>|单位为毫秒，脚本暂停执行的时间长度

返回值|类型|说明
-|-|-
nil|nil|nil

**语法：**

```lua
mSleep(interval)
```

**脚本示例：**

```lua
--延迟5秒
mSleep(5000)
```

# getUUID

### **设备唯一标识符(卸载重置，非UDID)**

参数|类型|说明
-|-|-
nil|nil|nil

返回值|类型|说明
-|-|-
UUID|<font color=#FF8C00>string</font>|App本地识别码(卸载重置，非UDID)

**语法：**

```lua
local UUID = getUUID()
```

# sysLog

### **系统日志**

参数|类型|说明
-|-|-
content|<font color=#FF8C00>string</font>|需要显示的日志内容

返回值|类型|说明
-|-|-
nil|nil|nil

**语法：**

```lua
sysLog(contents)
```

**脚本示例：**

```lua
sysLog("hello world!")
```

# lua_exit

### **退出脚本执行**

参数|类型|说明
-|-|-
nil|nil|nil

返回值|类型|说明
-|-|-
nil|nil|nil

**脚本示例：**

```lua
print("1")
lua_exit() --结束
print("2") --无法到达
```

# base64encode

### **Base64编码**

参数|类型|说明
-|-|-
str|<font color=#FF8C00>string</font>|字符串

返回值|类型|说明
-|-|-
base64|<font color=#FF8C00>string</font>|Base64 字符串

**语法：**

```lua
local base64 = base64encode(str)
```

**脚本示例：**

```lua
local base64 = base64encode("hello")
print(base64) -- aGVsbG8=
```

# base64decode

### **Base64解码**

参数|类型|说明
-|-|-
base64|<font color=#FF8C00>string</font>|Base64 字符串

返回值|类型|说明
-|-|-
str|<font color=#FF8C00>string</font>|字符串

**语法：**

```lua
local str = base64decode(base64)
```

**脚本示例：**

```lua
local str = base64decode("aGVsbG8=")
print(str) -- hello
```

# aesEncrypt

### **AES 加密**

参数|类型|说明
-|-|-
str|<font color=#FF8C00>string</font>|字符串
key|<font color=#FF8C00>string</font>|加密密钥

返回值|类型|说明
-|-|-
aes_str|<font color=#FF8C00>string</font>|AES 加密密文

**语法：**

```lua
local aes_str = aesEncrypt(str,key)
```

**脚本示例：**

```lua
local aes_str = aesEncrypt("hello","abc")
print(aes_str) --d1QG4T2tivoi0Kiu3NEmZQ==
```

# aesDecrypt

### **AES 解密**

参数|类型|说明
-|-|-
aes_str|<font color=#FF8C00>string</font>|AES 加密密文
key|<font color=#FF8C00>string</font>|加密密钥

返回值|类型|说明
-|-|-
str|<font color=#FF8C00>string</font>|字符串

**语法：**

```lua
local str = aesDecrypt(aes_str,key)
```

**脚本示例：**

```lua
local str = aesEncrypt("d1QG4T2tivoi0Kiu3NEmZQ==","abc")
print(str) --hello
```

# showUI

### **实时获取App UI 编辑框文本内容**

**使用说明：**

+ 脚本运行时，也可以实时获取AppUI编辑框文本。

参数|类型|说明
-|-|-
nil|nil|nil

返回值|类型|说明
-|-|-
str|<font color=#FF8C00>string</font>|UI编辑框文本内容

**脚本示例：**

```lua
local YXZHUSHOU_UI_TEXT = showUI()
```

# UI可视化

### **自动解析 UI.json 文件生成 UI 窗口**

`v1.0.7 UI bug问题将在 v1.0.8 修复`  

`问题1：UI多选框无法选择第一个参数，临时解决方案：第一个选项直接设置无。`

`问题2：UI下拉框无法正确返回参数编号，临时解决方案：下拉框使用相同名称选项，请使用相同序号排列，否则识别到其他位置，返回参数则是其他序号`

##### 页面 Page

要特别注意的是：如果存在Page控件，那么别的同级控件都就只能是Page控件，否则会导致UI解析出错，返回长度为0的字符串作为结果。

说明：生成一个独立页面到UI上，该页面可自由添加若干个控件，在上一级页面有Tab的页面索引，可点击切换页面，一共有三个属性：  

key|类型|说明
-|-|-
text|<font color=#FF8C00>string</font>|Tab标题
type|<font color=#FF8C00>string</font>|Page类型
views|<font color=#FFff0>Array</font>|控件容器

##### UI返回值  

除了标签 Label，页面Page 其余四种控件均存在文本型返回值，按照定义时的id，会返回以id为key的 json 类型数据。单选框返回当前选中项的编号；编辑框返回其中的内容；多选框返回 当前选中项的编号（从 0 开始），多个选项以 @ 分割。如：3@5 表示多选框组编号为 3 和 5 的两个选项已被选中。  

# getScreenSize

### **获取当前截图竖屏分辨率**

参数|类型|说明
-|-|-
nil|nil|nil

返回值|类型|说明
-|-|-
width|<font color=#00FFFF>number</font>|获取到的屏幕竖直（Home 键在下方时）宽度
height|<font color=#00FFFF>number</font>|获取到的屏幕竖直（Home 键在下方时）高度

**语法：**

```lua
local width,height = getScreenSize()
```

**脚本示例：**

```lua
local width,height = getScreenSize()
print("竖屏 宽 = " .. width)
print("竖屏 高 = " .. height)
```

# getMobilephoneType
### **获取当前手机型号**

参数|类型|说明
-|-|-
nil|nil|nil

返回值|类型|说明
-|-|-
iPhone|<font color=#FF8C00>string</font>|手机型号

**语法：**

```lua
local iPhone = getMobilephoneType()
```

**脚本示例：**

```lua
local iPhone = getMobilephoneType()
print(iPhone) 

--iPhone 2G
--iPhone 3G
--iPhone 3GS
--iPhone 4
--iPhone 4S
--iPhone 5
--iPhone 5c
--iPhone 5s
--iPhone 6 Plus
--iPhone 6
--iPhone 6s
--iPhone 6s Plus
--iPhone SE
--iPhone 7
--iPhone 7 Plus
--iPhone 8
--iPhone 8 Plus
--iPhone X
--iPhone XR
--iPhone XS
--iPhone XS Max
--iPhone 11
--iPhone 11 Pro
--iPhone 11 Pro Max
--iPod Touch 1G
--iPod Touch 2G
--iPod Touch 3G
--iPod Touch 4G
--iPod Touch 5G
--iPad 1G
--iPad 2
--iPad Mini 1G
--iPad 3
--iPad 4
--iPad Air
--iPad Mini 2G
--iPad Mini 3
--iPad Mini 4
--iPad Air 2
--iPad Pro 9.7
--iPad Pro 12.9
--iPhone Simulator  //模拟器
```

# WriteClipboard

### **文本写入剪切板**  `仅支持部分系统` 

说明：粘贴失败则表示不支持该系统( 实测暂不支持系统：iOS15 )

参数|类型|说明
-|-|-
text|<font color=#FF8C00>string</font>|文本内容

返回值|类型|说明
-|-|-
nil|nil|nil

**语法：**

```lua
WriteClipboard(text)
```

**脚本示例：**

```lua
--粘贴失败则表示不支持该系统( 实测暂不支持系统：iOS15 )
WriteClipboard("hello")  --写入剪切板
Mouse:KeyboardClick({"Win","v"});  --粘贴
```

# socket.http

### **http协议** 

LuaSocket [扩展库下载 ](https://gitee.com/lua_development/yxzhushou/tree/master/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8BLua%E7%AC%AC%E4%B8%89%E6%96%B9%E5%BA%93)

**POST 示例：**

```lua
local socket = require("socket.socket") --需要下载socket 扩展库
local http = require("socket.http")
local request_body = [[login=user&password=123]]

local response_body = {}
local res, code, response_headers =
http.request {
	 url = "http://httpbin.org/post", --网址
	 method = "POST", --协议
	 headers = { --请求头部
	 ["Content-Type"] = "application/x-www-form-urlencoded",
	 ["Content-Length"] = #request_body
	 },
	 source = ltn12.source.string(request_body), --请求数据
	 sink = ltn12.sink.table(response_body)
}
print(res)
print(code)
if type(response_headers) == "table" then
	 for k, v in pairs(response_headers) do
		print(k, v)
	 end
end
print("Response body:")
if type(response_body) == "table" then
 	print(table.concat(response_body)) --服务器数据返回
else
 	print("Not a table:", type(response_body))
end
```

**物联网鼠标 POST 示例：**

```lua
local socket = require("socket.socket") --需要下载socket 扩展库
local http = require("socket.http")
local request_body = '{"API":"KeyboardClick","data":["l"]}' --键盘输入 l
local response_body = {}

mSleep(10000) --10秒后
for i = 1,10 do --发送10次命令
	local res, code, response_headers =
	 http.request {
		 url = "http://api.heclouds.com/cmds?device_id=123456782", --设备服务器ID
		 method = "POST",
		 headers = {
		 ["Content-Type"] = "application/json",
		 ["Content-Length"] = #request_body,
		 ["api-key"] = "Q=hxaabvcdfghSb4HvZM7fNX8=", --设备服务器api-key
		 },
		 source = ltn12.source.string(request_body),
		 sink = ltn12.sink.table(response_body)
	}
end
```

# Mouse:new

### **初始化设备配置**

Mouse.lua [扩展库下载 ](https://gitee.com/lua_development/yxzhushou/tree/master/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8BLua%E7%AC%AC%E4%B8%89%E6%96%B9%E5%BA%93)

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00FFFF>number</font>|鼠标 io
device_id|<font color=#FF8C00>string</font>|物联网设备ID
key|<font color=#00FFFF>number</font>|物联网设备api-key
IP|<font color=#FF8C00>string</font>|局域网IP地址
Log|<font color=#00FFFF>number</font>|日志功能 开启：true 关闭：false

返回值|类型|说明
-|-|-
nil|nil|nil

**脚本示例：**

```lua
local Mouse = require("Mouse")
--例子1
Mouse:new({
	device_id = "896539182", --物联网鼠标 设备服务器ID
	key = "O3OdFlHoJCRtdZChXIbGVc87Ym0=", --物联网鼠标 设备服务器api-key
	io = 1                               -- 默认1号鼠标
})

--例子2
Mouse:new({
	IP = "192.168.3.3", --局域网IP地址
	io = 2                               -- 默认2号鼠标
})
```

# Mouse:MouseDown

### **鼠标按下**

函数说明：一直按住不放，需要搭配 [Mouse:MouseUp](#mouseup) 鼠标抬起

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
click|<font color=#FF8C00>string</font>|"左键"（默认）、"中键"（滚轮键）、"右键"

**脚本示例：**

```lua
--鼠标按住
Mouse:MouseDown();
```

# Mouse:MouseMove

### **鼠标移动(相对坐标)**

函数说明：从当前鼠标位置开始偏移像素点

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

 ESP8266物联网鼠标电脑串口调试 [视频教程](https://www.bilibili.com/video/BV1vP4y1w7xU/?spm_id_from=333.788) 
 
 开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
x|<font color=#00ffff>number</font>|水平相对移动 (负数 = 向左，正数 = 向右)
y|<font color=#00ffff>number</font>|垂直相对移动 (负数 = 向上，正数 = 向下)
z|<font color=#00ffff>number</font>|滚轮滚动 (负数 = 向上，正数 = 向下) [滚轮演示教程](https://www.bilibili.com/video/BV1dF411g749?spm_id_from=333.999.0.0)

**脚本示例：**

```lua
--鼠标相对移动
Mouse:MouseMove({x = -30,y = -30});
```

# Mouse:MouseUp

### **鼠标抬起**

函数说明：抬起，需要搭配 [MouseDown](#mousedown) 鼠标按下

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)

**脚本示例：**

```lua
--鼠标抬起
Mouse:MouseUp();
```

# Mouse:MouseClick

### **鼠标单击/鼠标长按**

函数说明：按下后开始计时，自动抬起

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
sleep|<font color=#00ffff>number</font>|单位：毫秒 (1000 = 1秒)
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"

**脚本示例：**

```lua
--鼠标单击/鼠标长按
Mouse:MouseClick({sleep = 5000});--5秒
```

# Mouse:MouseSlide

### **鼠标滑动**

函数说明：x,y 起点坐标，x1,y1 终点坐标

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
x|<font color=#00ffff>number</font>|起点x
y|<font color=#00ffff>number</font>|起点y
x1|<font color=#00ffff>number</font>|终点x
y1|<font color=#00ffff>number</font>|终点y
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"

**脚本示例：**

```lua
--滑动
Mouse:MouseSlide({x = 365, y = 867, x1 = 370, y1 = 266});
```

# Mouse:KeyboardClick

### **键盘输入**

函数说明：模拟键盘输入，最多支持同时按下 6 个键

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|参数1
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
Keyboard1|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard2|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard3|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard4|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard5|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard6|<font color=#FF8C00>string</font>|键盘值（最多6个）

**脚本示例：**

```lua
--键盘输出（默认：1）
Mouse:KeyboardClick({"k"}); --输入小写k
Mouse:KeyboardClick({"K"}); --输入大写k
Mouse:KeyboardClick({"Shift","k"}); --输入大写k
Mouse:KeyboardClick({"Space"}); --输入空格键
Mouse:KeyboardClick({"\\"}); --反斜杠 \
Mouse:KeyboardClick({"\'"}); --单引号 '
Mouse:KeyboardClick({"?"}); --问号
Mouse:KeyboardClick({"Win","c"});  --Mac、iOS 复制
Mouse:KeyboardClick({"Win","v"});  --Mac、iOS 粘贴
Mouse:KeyboardClick({"Ctrl","c"});  --Windows 复制
Mouse:KeyboardClick({"Ctrl","v"});  --Windows 粘贴
```

# Mouse:touchMove

### **鼠标移动(绝对坐标)**

函数说明：初始化鼠标位置到左上角后开始偏移像素点

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

开发教程 [视频教程](https://www.bilibili.com/video/bv1nu411X7fw)

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
x|<font color=#00ffff>number</font>|水平绝对移动
y|<font color=#00ffff>number</font>|垂直绝对移动
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"

**脚本示例：**

```lua
--鼠标移动
Mouse:touchMove({x = 639, y = 1245});
```